﻿using System;
using PromotionEngine.Patterns;
using PromotionEngine.Models.Concrete;
using PromotionEngine.Enums;
using System.Linq;
using System.Collections.Generic;
using PromotionEngine.Models.Interfaces;

namespace PromotionEngine
{

    class Program
    {

        static void Main(string[] args)
        {
            List<Product> products = new List<Product>()
            {
                new Product{ Code= EProductTypes.A, Name="SKU A", Quantity=3, Price=50M},
                new Product{ Code= EProductTypes.B, Name="SKU B", Quantity=5, Price=30M },
                new Product{ Code= EProductTypes.C, Name="SKU C", Quantity=1, Price=20M },
                new Product{ Code= EProductTypes.D, Name="SKU D", Quantity=1, Price=15M }
            };

            var tmpProducts = new List<Product>();
            foreach (Product prd in products)
            {
                if (prd.Code == EProductTypes.C || prd.Code == EProductTypes.D)
                {
                    if (tmpProducts.Count > 0)
                    {
                        tmpProducts.Clear();
                        tmpProducts.Add(new Product
                        {
                            Code = EProductTypes.CD,
                            Name = "CD",
                            Quantity = 1,
                            Price = 30
                        });
                    }

                    else
                        tmpProducts.Add(prd);
                }
            }

            Cart cart = new Cart()
            { Products = products, BasePrice = 0M, PromotedPrice = 0M, TotalPrice = 0M };


            List<IPromotion> promotions = new List<IPromotion>{
            new Promotion { ProductCode = EProductTypes.A, MinQuantity = 3, Discounts = 130M, Expires = DateTime.Now.AddDays(2) },
            new Promotion { ProductCode=EProductTypes.B , MinQuantity=2, Discounts=45M, Expires=DateTime.Now.AddDays(2)},
            new Promotion { ProductCode = EProductTypes.CD, MinQuantity=1, Discounts=30M, Expires=DateTime.Now.AddDays(2)}
            };

            IPromotionFactory promotionFactory;
            cart.BasePrice = products.Sum(p => p.Quantity * p.Price);
            cart.TotalPrice = cart.BasePrice - cart.PromotedPrice;
            Console.WriteLine("Before calculating Discounts");
            Console.WriteLine("Base Price {0}, Discounts {1}, TotalPrice {2}", cart.BasePrice, cart.PromotedPrice, cart.TotalPrice);

            if (tmpProducts.Count > 0)
            {
                Product prd = tmpProducts.Where(p => p.Code == EProductTypes.CD).FirstOrDefault();
                promotionFactory = new PromotionFactory(prd, promotions);
                cart.PromotedPrice += promotionFactory.GetPromotion(EProductTypes.CD);
            }

            foreach (Product _product in cart.Products)
            {
                if (!(_product.Code == EProductTypes.C || _product.Code == EProductTypes.D) && tmpProducts.Count > 0)
                {
                    promotionFactory = new PromotionFactory(_product, promotions);
                    cart.PromotedPrice += promotionFactory.GetPromotion(_product.Code);
                }
            }
            cart.TotalPrice = cart.BasePrice - cart.PromotedPrice;
            Console.WriteLine("After calculating Discounts");
            Console.WriteLine("Base Price {0}, Discounts {1}, TotalPrice {2}", cart.BasePrice, cart.PromotedPrice, cart.TotalPrice);
            Console.ReadLine();
        }


    }
}
