﻿using PromotionEngine.BusinessLogic.Interface;
using PromotionEngine.Models.Interfaces;
using System;

namespace PromotionEngine.BusinessLogic.Concrets
{
   public class PromotionRuleCD : IPromotionRule
    {
        public PromotionRuleCD()
        {

        }
        public decimal CalculateDiscounts(IProduct product, IPromotion promotion)
        {
            if (product == null || promotion == null)
                return 0M;

            decimal returnPrice = 0M;

            if (product?.Quantity < promotion.MinQuantity)
                returnPrice = 0M;

            else
            {
                if (promotion.Expires > DateTime.Now)
                {           
                    return 5; // (c + d) = >   20+15 =35  but we are paid only 30 to 35-30 = 5
                }
            }
            return returnPrice;
        }

    }
}
