﻿using PromotionEngine.BusinessLogic.Interface;
using PromotionEngine.Models.Interfaces;
using System;

namespace PromotionEngine.BusinessLogic.Concrets
{
   public class PromotionRuleB : IPromotionRule
    {
        public PromotionRuleB()
        {

        }
        public decimal CalculateDiscounts(IProduct product, IPromotion promotion)
        {
            if (product == null || promotion == null)
                return 0M;

            decimal returnPrice = 0M;

            if (product?.Quantity < promotion.MinQuantity)
                returnPrice = 0M;

            else
            {
                if (promotion.Expires > DateTime.Now)
                {
                    int reminder;
                    int sets = Math.DivRem(product.Quantity, promotion.MinQuantity, out reminder);
                    returnPrice = (product.Quantity * product.Price) - ((sets * promotion.Discounts) + (reminder * product.Price));
                }
            }
            return returnPrice;
        }

    }
}
