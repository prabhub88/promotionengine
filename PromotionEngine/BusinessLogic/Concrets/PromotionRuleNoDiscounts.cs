﻿using PromotionEngine.BusinessLogic.Interface;
using PromotionEngine.Models.Interfaces;
using System;

namespace PromotionEngine.BusinessLogic.Concrets
{
   public class PromotionRuleNoDiscounts : IPromotionRule
    {
        public PromotionRuleNoDiscounts()
        {

        }

        public decimal CalculateDiscounts(IProduct product, IPromotion promotion)
        {
            if (product == null || promotion == null)
                return 0M;

           return  product.Price * product.Quantity;

        }
    }
}
