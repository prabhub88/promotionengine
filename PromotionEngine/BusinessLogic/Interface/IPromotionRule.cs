﻿using PromotionEngine.Models.Interfaces;

namespace PromotionEngine.BusinessLogic.Interface
{
    public interface IPromotionRule
    {
        public decimal CalculateDiscounts(IProduct product,IPromotion promotion);
    }
}
