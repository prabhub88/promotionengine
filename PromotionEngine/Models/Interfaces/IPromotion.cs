﻿using System;

namespace PromotionEngine.Models.Interfaces
{
   public interface IPromotion
    {
        public PromotionEngine.Enums.EProductTypes ProductCode { get; }
        public int MinQuantity { get; }
        public decimal Discounts { get; }
        public DateTime Expires { get; }
    }
}
