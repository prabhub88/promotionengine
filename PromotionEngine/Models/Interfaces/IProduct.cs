﻿namespace PromotionEngine.Models.Interfaces
{
    public interface IProduct
    {
        public PromotionEngine.Enums.EProductTypes Code { get; }
        public string Name { get; }
        public decimal Price { get; }
        public int Quantity { get; }
    }
}
