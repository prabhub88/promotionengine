﻿using System.Collections.Generic;
using PromotionEngine.Models.Concrete;

namespace PromotionEngine.Models.Interfaces
{
    public interface ICart
    {
        public List<Product> Products   { get; }
        public decimal BasePrice { get; }
        public decimal PromotedPrice { get; }
        public decimal TotalPrice { get; }
    }
}
