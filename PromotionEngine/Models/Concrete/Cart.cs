﻿using PromotionEngine.Models.Interfaces;
using System.Collections.Generic;

namespace PromotionEngine.Models.Concrete
{
    public class Cart : ICart
    {
        public List<Product> Products { get; set; }
        public decimal BasePrice { get; set; }
        public decimal PromotedPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
