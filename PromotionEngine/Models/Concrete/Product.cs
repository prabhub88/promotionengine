﻿using PromotionEngine.Models.Interfaces;

namespace PromotionEngine.Models.Concrete
{
  public  class Product : IProduct
    {
        public PromotionEngine.Enums.EProductTypes Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

    }
}
