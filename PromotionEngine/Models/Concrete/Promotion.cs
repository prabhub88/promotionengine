﻿using System;
using PromotionEngine.Models.Interfaces;

namespace PromotionEngine.Models.Concrete
{
   public class Promotion : IPromotion
    {
        public PromotionEngine.Enums.EProductTypes ProductCode { get; set; }
        public int MinQuantity { get; set; }
        public decimal Discounts { get; set; }
        public DateTime Expires { get; set; }
    }
}
