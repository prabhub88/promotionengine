﻿using PromotionEngine.Enums;
using PromotionEngine.BusinessLogic.Interface;
using PromotionEngine.BusinessLogic.Concrets;
using PromotionEngine.Models.Interfaces;
using PromotionEngine.Models.Concrete;
using System.Collections.Generic;
using System.Linq;

namespace PromotionEngine.Patterns
{
    public class PromotionFactory : IPromotionFactory
    {
        IProduct product;
      List<IPromotion> promotions;

        public PromotionFactory(Product _product, List<IPromotion> _promotion)
        {
            this.product = _product;
            this.promotions = _promotion;
        }
        public decimal GetPromotion(EProductTypes productTypes)
        {
            IPromotionRule promotionRule;
            switch (productTypes)
            {
                case EProductTypes.A:
                    promotionRule = new PromotionRuleA();
                    break;
                case EProductTypes.B:
                    promotionRule = new PromotionRuleB();
                    break;
                case EProductTypes.CD:
                    promotionRule = new PromotionRuleCD();
                    break;
                default:
                    promotionRule = new PromotionRuleNoDiscounts();
                    break;
            }
            var currentPromotion = promotions.Where(prm => prm.ProductCode == productTypes).FirstOrDefault();
            return  promotionRule.CalculateDiscounts(product, currentPromotion );
        }
    }
}
