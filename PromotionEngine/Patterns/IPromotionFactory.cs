﻿using PromotionEngine.BusinessLogic.Interface;
using PromotionEngine.Enums;

namespace PromotionEngine.Patterns
{
    public interface IPromotionFactory
    {
        public decimal GetPromotion(EProductTypes  productTypes);
    }
}
