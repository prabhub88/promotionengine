using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PromotionEngine.BusinessLogic.Interface;
using PromotionEngine.BusinessLogic.Concrets;
using PromotionEngine.Models.Concrete;
using PromotionEngine.Models.Interfaces;
using PromotionEngine.Enums;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Linq;

namespace PromotionEngineTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestPromotionRuleA()
        {
            var ruleA = new Mock<PromotionRuleA>();
            var resp=  ruleA.Object.CalculateDiscounts(It.IsAny<IProduct>(), It.IsAny<IPromotion>());
            Assert.AreEqual(0M, resp);
        }

        [TestMethod]
        public void TestPromotionRuleA_For2_Quantity()
        {
            List<IPromotion> promotions = new List<IPromotion>{
            new Promotion { ProductCode = EProductTypes.A, MinQuantity = 3, Discounts = 130M, Expires = DateTime.Now.AddDays(2) },
            new Promotion { ProductCode=EProductTypes.B , MinQuantity=2, Discounts=45M, Expires=DateTime.Now.AddDays(2)},
            new Promotion { ProductCode = EProductTypes.CD, MinQuantity=1, Discounts=30M, Expires=DateTime.Now.AddDays(2)}
            };

            List<Product> products = new List<Product>()
            {
                new Product{ Code= EProductTypes.A, Name="SKU A", Quantity=2, Price=50M},
                new Product{ Code= EProductTypes.B, Name="SKU B", Quantity=7, Price=30M },
                new Product{ Code= EProductTypes.C, Name="SKU C", Quantity=1, Price=20M },
                new Product{ Code= EProductTypes.C, Name="SKU C", Quantity=1, Price=15M }
            };

            var ruleA = new Mock<PromotionRuleA>();
            var resp = ruleA.Object.CalculateDiscounts(products.Where(p=> p.Code==EProductTypes.A).FirstOrDefault(), promotions.Where(p=>p.ProductCode==EProductTypes.A).FirstOrDefault());
            Assert.AreEqual(100M, resp);
        }
        [TestMethod]
        public void TestPromotionRuleA_For_discount_Quantity()
        {
            List<IPromotion> promotions = new List<IPromotion>{
            new Promotion { ProductCode = EProductTypes.A, MinQuantity = 3, Discounts = 130M, Expires = DateTime.Now.AddDays(2) },
            new Promotion { ProductCode=EProductTypes.B , MinQuantity=2, Discounts=45M, Expires=DateTime.Now.AddDays(2)},
            new Promotion { ProductCode = EProductTypes.CD, MinQuantity=1, Discounts=30M, Expires=DateTime.Now.AddDays(2)}
            };

            List<Product> products = new List<Product>()
            {
                new Product{ Code= EProductTypes.A, Name="SKU A", Quantity=5, Price=50M},
                new Product{ Code= EProductTypes.B, Name="SKU B", Quantity=7, Price=30M },
                new Product{ Code= EProductTypes.C, Name="SKU C", Quantity=1, Price=20M },
                new Product{ Code= EProductTypes.C, Name="SKU C", Quantity=1, Price=15M }
            };

            var ruleA = new Mock<PromotionRuleA>();
            var resp = ruleA.Object.CalculateDiscounts(products.Where(p => p.Code == EProductTypes.A).FirstOrDefault(), promotions.Where(p => p.ProductCode == EProductTypes.A).FirstOrDefault());
            Assert.AreEqual(20M, resp);
        }

    }
}
